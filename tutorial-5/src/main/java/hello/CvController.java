package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {

    @GetMapping("/curriculumvitae")
    public String curriculumvitae(@RequestParam(name = "visitor", required = false)
                                   String visitor, Model model) {
////        if (name.equals(""){
//            model.addAttribute(name + " I hope you interested to hire me");
//        } else {
//            model.addAttribute("This is My CV");
//        }
        model.addAttribute("visitor", visitor);
        return "cv";
    }

}