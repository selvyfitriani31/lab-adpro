package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class EdamCheese implements Cheese {

    public String toString() {
        return "Shredded Edam";
    }
}
