package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NewYorkPizzaStore();

        Pizza pizza_ny = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza_ny + "\n");

        pizza_ny = nyStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza_ny + "\n");

        pizza_ny = nyStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza_ny + "\n");

        // TODO Complete me!
        // Create a new Pizza Store franchise at Depok
        PizzaStore deStore = new DepokPizzaStore();

        Pizza pizza_de = deStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza_de + "\n");

        pizza_de = deStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza_de + "\n");

        pizza_de = deStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza_de + "\n");

    }
}
