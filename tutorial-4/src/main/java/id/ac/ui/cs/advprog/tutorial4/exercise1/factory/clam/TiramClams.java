package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class TiramClams implements Clams {

    public String toString() {
        return "Tiram Clams from Indonesian sea";
    }
}
