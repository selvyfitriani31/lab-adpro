package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class SquashBlossoms implements Veggies {

    public String toString() {
        return "Squash Blossoms";
    }
}
