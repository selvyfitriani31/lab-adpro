package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck{
    // TODO Complete me!
    public MallardDuck() {
        QuackBehavior quackBehavior = new Quack();
        FlyBehavior flyBehavior = new FlyWithWings();
    }

    @Override
    public void display(){
        System.out.println("I'm a real Mallard Duck");
    }


}
